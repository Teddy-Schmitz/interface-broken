package main

import (
	"database/sql"

	"gopkg.in/mgutz/dat.v2/dat"
	_ "gopkg.in/mgutz/dat.v2/postgres"
	"gopkg.in/mgutz/dat.v2/sqlx-runner"
)

type DBQueryable interface {
	Call(sproc string, args ...interface{}) *dat.CallBuilder
	DeleteFrom(table string) *dat.DeleteBuilder
	Exec(cmd string, args ...interface{}) (*dat.Result, error)
	ExecBuilder(b dat.Builder) error
	ExecMulti(commands ...*dat.Expression) (int, error)
	InsertInto(table string) *dat.InsertBuilder
	Insect(table string) *dat.InsectBuilder
	Select(columns ...string) *dat.SelectBuilder
	SelectDoc(columns ...string) *dat.SelectDocBuilder
	SQL(sql string, args ...interface{}) *dat.RawBuilder
	Update(table string) *dat.UpdateBuilder
	Upsert(table string) *dat.UpsertBuilder
}

func test(db DBQueryable) {
	db.SQL("test")
}

func main() {
	db, _ := sql.Open("postgres", "postgres://dev:dev@localhost/dev?sslmode=disable")
	datdb := runner.NewDB(db, "postgres")
	trx, _ := datdb.Begin()
	test(trx)
}
